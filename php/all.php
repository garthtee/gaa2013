<?php

	if(isset($_COOKIE['username']) == 'root') { // user is logged-in

?>

<!DOCTYPE html>
<html>
<head>
	<title>All GAA Info 13'</title>
</head>
<style>
table {
	width: 100%;
}
table, th, td {
	border: 1px solid black;
}
th {
	background: #6fe;
}
th, td {
    padding: 5px;
    text-align: center;    
}
h1 {
	text-align: center;
	padding-top: 10%;
}
h3{
	padding-left: 45%;
}
body {
	padding-left: 10%;
	padding-right: 10%;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: #4d4d4d;
    font-size: 25px;
}
li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
/* Change the link color on hover */
li a:hover {
    background-color: #6FE;
    color: #000;
}

</style>
<body>

	<!-- Navigation Bar -->
	<ul>
	<li><a href="all.php">All</a></li>
	<li><a href="categories.php">Categories</a></li>
	<li><a href="week.php">Week</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>
	<!-- End Navigation Bar -->

	<h1>Recent games..</h1>

	<table>

			<!-- Display all results -->
<?php

	if(isset($_COOKIE['username']) == 'root') {
		$connection = mysqli_connect("localhost", "root", "");
		mysqli_select_db($connection, "gaa2013");
		$result = mysqli_query($connection, "select * from games");
		print("<tr><th>DateTime</th><th>Team 1</th><th>Results</th><th>Team 2</th></tr>");
		while ($row = mysqli_fetch_array($result)) {
			print("<tr>");
			print("<td>");
			print($row['datetime']);
			print("</td>");
			print("<td>");
			print($row['team1']);
			print("</td>");
			print("<td>");
			print($row['team1gls']);
			print("-");
			print($row['team1pts']);
			print("    -   ");
			print($row['team2gls']);
			print("-");
			print($row['team2pts']);
			print("</td>");
			print("<td>");
			print($row['team2']);
			print("</td>");
			print("</tr>");
		}
		mysqli_close($connection);
	} else {
		print("<h1>Unathorised access to Admin page.</h1>");
	}
?>
</html>
</body>

<?php

	} else { // user not logged-in
		print("<h1>Unathorised access to Admin page.</h1>");
		print('<a href="../html/login.html">Return to login..</h1>');
	}

?>
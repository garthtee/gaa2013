<?php

	if(isset($_GET['gameWeekIn'])) {
		$weekIn = $_GET['gameWeekIn'];
	} else {
		$weekIn = 1;
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Weeklys</title>
</head>
<style>
table {
	width: 100%;
}
table, th, td {
	border: 1px solid black;
}
th {
	background: #6fe;
}
th, td {
    padding: 5px;
    text-align: center;    
}
h1 {
	text-align: center;
	padding-top: 10%;
}
h3{
	padding-left: 45%;
}
body {
	padding-left: 10%;
	padding-right: 10%;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: #4d4d4d;
    font-size: 25px;
}
li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
/* Change the link color on hover */
li a:hover {
    background-color: #6FE;
    color: #000;
}

.weekLink {
	text-decoration: none;  /* Remove underline */ 
	font-size: 20px;
	padding-left: 45%; /* Center*/
}

</style>
<body>

	<!-- Navigation Bar -->
	<ul>
	<li><a href="all.php">All</a></li>
	<li><a href="categories.php">Categories</a></li>
	<li><a href="week.php">Week</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>
	<!-- End Navigation Bar -->

	<?php 
		print('<h1>Week '. $weekIn . '</h1>'); 
	?>

	<table>

			<!-- Display weekly games depending on week passed in -->
<?php
		
		$connection = mysqli_connect("localhost", "root", "");
		mysqli_select_db($connection, "gaa2013");
		$result = mysqli_query($connection, "select * from games where gameweek = $weekIn;");
		// Calculating max week amount //
			$maxWeekQuery = mysqli_query($connection, "select MAX(gameweek) AS max from games;"); // selects the largest week amount and stores in max
			$row1 = mysqli_fetch_array($maxWeekQuery);
			$maxWeek = $row1['max'];
		// End Calculating max week amount //
		print("<tr><th>DateTime</th><th>Team 1</th><th>Results</th><th>Team 2</th></tr>");
		$numRows = mysqli_num_rows($result);
		while ($row = mysqli_fetch_array($result)) {
			print("<tr>");
			print("<td>");
			print($row['datetime']);
			print("</td>");
			print("<td>");
			print($row['team1']);
			print("</td>");
			print("<td>");
			print($row['team1gls']);
			print("-");
			print($row['team1pts']);
			print("    -   ");
			print($row['team2gls']);
			print("-");
			print($row['team2pts']);
			print("</td>");
			print("<td>");
			print($row['team2']);
			print("</td>");
			print("</tr>");
		}
		mysqli_close($connection);
		
?>
</table>

		<!-- Adding bottom links [forward week and back week] -->
<?php
		
			$forwardArrow="&#62;"; // HTML value of '>'
			$backArrow="&#60;"; // HTML value of '<'

			print('<br>');
			if(($weekIn-1) != 0) {
			print('<a class="weekLink" href="week.php?gameWeekIn=' . ($weekIn-1) .  '">' .
			 $backArrow . $backArrow . ' View week ' . ($weekIn-1) .'</a>');
			print('<br>');
			if($weekIn != $maxWeek) {
				print('<a class="weekLink" href="week.php?gameWeekIn=' . ($weekIn+1) . '">View week ' . 
					($weekIn+1) . ' ' . $forwardArrow . $forwardArrow . '</a>');
			}
			} else {
				print('<a class="weekLink" href="week.php?gameWeekIn=' . ($weekIn+1) . '">View weeks ' . 
				($weekIn+1) . ' ' . $forwardArrow . $forwardArrow . '</a>');
			}
?>
</body>
</html>
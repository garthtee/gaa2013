<?php

	if(isset($_COOKIE['username']) == 'root') { // user is logged-in

?>

<!DOCTYPE html>
<html>
<head>
	<title>Categories</title>
</head>
<style>
table {
	width: 100%;
}
table, th, td {
	border: 1px solid black;
}
th {
	background: #6fe;
}
th, td {
    padding: 5px;
    text-align: center;    
}
h1 {
	text-align: center;
	padding-top: 10%;
}
h3{
	padding-left: 45%;
}
body {
	padding-left: 10%;
	padding-right: 10%;
}
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: #4d4d4d;
    font-size: 25px;
}
li a {
    display: block;
    color: white;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
}
/* Change the link color on hover */
li a:hover {
    background-color: #6FE;
    color: #000;
}
select {
	width: 100%;
}

</style>
<body>

	<!-- Navigation Bar -->
	<ul>
	<li><a href="all.php">All</a></li>
	<li><a href="categories.php">Categories</a></li>
	<li><a href="week.php">Week</a></li>
	<li><a href="logout.php">Logout</a></li>
	</ul>
	<!-- End Navigation Bar -->

	<table>

		<!-- Select categories for dropdown menu -->
<?php

	

		print('<form method="POST" action="categories.php">');

		$connection = mysqli_connect("localhost", "root", "");
		mysqli_select_db($connection, "gaa2013");
		$result = mysqli_query($connection, "select * from categories");
		print('<br><hr><br>');
		print('<select name="selectedOption" onchange="this.form.submit()">');
		print('<option selected disabled>Select Category</option>');
		while ($row = mysqli_fetch_array($result)) {
			print("<option>");
			print($row['id'] . ' ' . $row['description']);
			print("</option>");
		}
		mysqli_close($connection);
		print("</select>");
		print('</form>');

		print('<br><br><hr>');
	
?>
		<!-- Display results depending on what was chosen in dropdown menu -->
<?php
		if(isset($_POST['selectedOption'])) {
			$selectedOption = $_POST['selectedOption'];
			$connection = mysqli_connect("localhost", "root", "");
			mysqli_select_db($connection, "gaa2013");
			$result = mysqli_query($connection, "select * from games");
			print("<tr><th>DateTime</th><th>Team 1</th><th>Results</th><th>Team 2</th></tr>");
			print('<h1 id="categoryTitle">' . $selectedOption . '</h1>');
			while ($row = mysqli_fetch_array($result)) {
				$category=$row['category'];
				if(stristr($selectedOption, $category)) {
					print("<tr>");
					print("<td>");
					print($row['datetime']);
					print("</td>");
					print("<td>");
					print($row['team1']);
					print("</td>");
					print("<td>");
					print($row['team1gls']);
					print("-");
					print($row['team1pts']);
					print("    -   ");
					print($row['team2gls']);
					print("-");
					print($row['team2pts']);
					print("</td>");
					print("<td>");
					print($row['team2']);
					print("</td>");
					print("</tr>");
				}
			} // end while loop
			mysqli_close($connection);
		}
	 
?>
</html>
</body>

<?php

	} else { // user not logged-in
		print("<h1>Unathorised access to Admin page.</h1>");
		print('<a href="../html/login.html">Return to login..</h1>');
	}

?>
window.onload = function() { // Whenever webpage finishes loading
  initFunction();
}

function initFunction()
{
	document.getElementById("usernameID").onfocus = function() {
		validateUsername();
	}
	document.getElementById("passwordID").onfocus = function() {
		validatePassword();
	}
	document.getElementById("submitID").onclick = function() {
		onClickSubmit();
	}
}

function validateUsername() {
	document.getElementById("pID").innerHTML = "Username is root.";
}

function validatePassword() {
	document.getElementById("pID").innerHTML = "Password is... NOT!";
}

function onClickSubmit() {
	var usernameID = document.getElementById("usernameID");
	var passwordID = document.getElementById("passwordID");

	if(usernameID.value == "") {
		document.getElementById("pID").innerHTML = "Enter username";
	} else if(passwordID.value == "") {
		document.getElementById("pID").innerHTML = "Enter password";
	}
}